import Hapi from "@hapi/hapi";
import Nes from "@hapi/nes";
import { promises } from 'fs';

import { Client, IParsedMessage, IRC_TYPES } from "twsc";
import { Logger } from "./logger/logger";

// SFX URI
const baseUri: string = "https://storage.gra.cloud.ovh.net/v1/AUTH_0697890e15ad4098b140de1fed916171/hregibo-static/stream/audio-sfx/";
const lastTriggeredAt: number = 0;

// browserified js
let clientCode: string = "";

const sfxCommands = new Map();
sfxCommands.set("!enculéé", `${baseUri}mais%20quel%20encule.mp3`);
sfxCommands.set("!zoidberg", `${baseUri}woutoutou.WAV`);
sfxCommands.set("!tenapas", `${baseUri}ten%20a%20pas.mp3`);
sfxCommands.set("!zbra", `${baseUri}zbra.wav`);
sfxCommands.set("!burp", `${baseUri}rot.wav`);
sfxCommands.set("!alextow", `${baseUri}hahaha.wav`);
sfxCommands.set("!hey", `${baseUri}heeeeeeey.wav`);
sfxCommands.set("!titanic", `${baseUri}titanic.mp3`);
sfxCommands.set("!tuturu", `${baseUri}tuturu.mp3`);
sfxCommands.set("!woo", `${baseUri}sekiro-woo.mp3`);
sfxCommands.set("!yeet", `${baseUri}yeet.mp3`);
sfxCommands.set("!sncf", `${baseUri}tam%20tam%20talam.wav`);
sfxCommands.set("!bisou", `${baseUri}bisou.ogg`);

const prepareServer = async () => {
    const server = new Hapi.Server({
        port: 17844,
        host: '0.0.0.0',
    });
    await server.register(Nes);
    server.route({
        "method": "GET",
        "path": "/",
        handler: (_r, h) => h.response(`
<html><head><title>SFX</title></head><body><script>${clientCode}</script></html>
`).code(200),
    });
    server.route({
        "method": "GET",
        "path": "/events/sfx",
        handler: (_r, h) => h.response({ok: true}).code(200),
    });
    server.route({
        "method": "GET",
        "path": "/events/volume",
        handler: (_r, h) => h.response({ok: true}).code(200),
    });
    server.subscription("/events/sfx");
    server.subscription("/events/volume");
    return server;
}

(async () => {
    clientCode = (await promises.readFile(`./client.js`)).toString();
    const HapiServer = await prepareServer();
    const chatListener = new Client({});
    chatListener.on("ready", () => {
        Logger.info("We are now listening!");
        chatListener.send("JOIN #lumikkode");
    });
    chatListener.on("message", (message: IParsedMessage) => {
        if (message.type !== IRC_TYPES.PRIVMSG) {
            return;
        }
        const theMessage = message.content.split(" ");
        const theCommand = theMessage.shift();
        const isMod = message.tags && message.tags.get("mod") === "1";
        const isVip = message.tags && message.tags.get("vip") === "1";
        const isBroadcaster = message.prefix &&message.prefix.get("user") === "lumikkode";
        Logger.info({
            isMod, isBroadcaster, isVip, theCommand, theMessage
        }, "Status of the requester")
        if (Date.now() < (lastTriggeredAt + 60000) && !isVip && !isMod) {
            Logger.info("Out of conditions to continue, skipping");
            return;
        }

        const isLumiTrigger = message.content.toLowerCase().indexOf('lumi') > -1;
        Logger.info({ msg: message.content }, "Lumi keyword triggered");
        if (isLumiTrigger) {
            HapiServer.publish("/events/sfx", {
                command: 'lumi',
                uri: `${baseUri}lumi.wav`,
            });
        }

        if (!message.content.startsWith("!")) {
            return;
        }

        if ((isMod || isBroadcaster) && theCommand === "!volume") {
            Logger.info("Requested a volume change");
            let volume = parseInt(theMessage.shift());
            if (isNaN(volume)) {
                Logger.error("Not a number, skipping");
                return;
            }
            if (volume > 100 || volume < 1) {
                Logger.error("Volume invalid range, skipping");
                return;
            }
            volume /= 100;
            Logger.info("Emitting volume change");
            HapiServer.publish("/events/volume", volume);
            return;
        }

        if (!sfxCommands.has(theCommand)) {
            Logger.info("Command sfx does not exist, skipping");
            return;
        }


        const payload = {
            command: theCommand,
            uri: sfxCommands.get(theCommand),
        };
        Logger.info("Emitting SFX event to the client");
        HapiServer.publish("/events/sfx", payload);
    });
    chatListener.onStart();
    await HapiServer.start();
    Logger.info("Server is now started.");
})();
