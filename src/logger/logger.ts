import * as Bunyan from "bunyan";

export const Logger = Bunyan.createLogger({
    name: "MHK-IRC",
    semver: "3.1.0",
    streams: [
        {
            level: "debug",
            stream: process.stdout,
            type: "stream",
        },
    ],
});
