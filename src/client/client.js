const Nes = require("@hapi/nes/lib/client");

const listener = new Nes.Client("ws://localhost:17844");

let requestedVolume = 1;
/** @type Map<string, HTMLAudioElement> */
const loadedSfx = new Map();
listener.subscribe("/events/sfx", (sfxData) => {
    console.log("Received sfx request", sfxData);
    if (!loadedSfx.has(sfxData.command)) {
        console.log("SFX not yet registered, inserting");
        const audioPlayerSfx = new Audio(sfxData.uri);
        loadedSfx.set(sfxData.command, audioPlayerSfx);
    }
    console.log("Playing the SFX now!");
    const requestedAudioEffect = loadedSfx.get(sfxData.command);
    requestedAudioEffect.volume = requestedVolume;
    requestedAudioEffect.play();
});
listener.subscribe("/events/volume", (volume) => {
    console.log("Volume request change to ", volume);
    requestedVolume = volume;
});
listener.connect();
window.listener = listener;