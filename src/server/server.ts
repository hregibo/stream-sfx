import Hapi from "@hapi/hapi";
import Nes from "@hapi/nes";

const prepareServer = async () => {
    const server = new Hapi.Server({
        port: 17844,
        host: '0.0.0.0',
    });
    await server.register(Nes);
    server.route({
        "method": "GET",
        "path": "/",
        handler: (_r, h) => { h.response({ok: true}).code(200); },
    });
    server.route({
        "method": "GET",
        "path": "/events/sfx",
        handler: (_r, h) => { h.response({ok: true}).code(200); },
    });
    server.route({
        "method": "GET",
        "path": "/events/volume",
        handler: (_r, h) => { h.response({ok: true}).code(200); },
    });
    server.subscription("/events/sfx");
    server.subscription("/events/volume");
    return server;
}
const server = prepareServer().then(s => s).catch(err => console.log("ERROR", err));
export { server };
export default server;
